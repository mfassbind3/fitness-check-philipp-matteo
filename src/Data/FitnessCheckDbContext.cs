using FitnessCheck.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace FitnessCheck.Data
{
    public class FitnessCheckDbContext : DbContext
    {
        // DbSet for storing Fitnesscheck entity data.
        public virtual DbSet<Medizinballstoss> Medizinballstoss { get; set; }
        public virtual DbSet<Einbeinstand> Einbeinstand { get; set; }
        public virtual DbSet<Pendellauf> Pendellauf { get; set; }
        public virtual DbSet<Rumpfkrafttest> Rumpfkrafttest { get; set; }
        public virtual DbSet<Standweitsprung> Standweitsprung { get; set; }
        public virtual DbSet<Zwolfminutenlauf> Zwolfminutenlauf { get; set; }

        public FitnessCheckDbContext(DbContextOptions<FitnessCheckDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Entity mapping for Medizinballstoss table
            modelBuilder.Entity<Medizinballstoss>(entity =>
            {
                entity.ToTable("Medizinballstoss");
            });

            // Entity mapping for Einbeinstand table
            modelBuilder.Entity<Einbeinstand>(entity =>
            {
                entity.ToTable("Einbeinstand");
            });

            // Entity mapping for Pendellauf table
            modelBuilder.Entity<Pendellauf>(entity =>
            {
                entity.ToTable("Pendellauf");
            });

            // Entity mapping for Rumpfkrafttest table
            modelBuilder.Entity<Rumpfkrafttest>(entity =>
            {
                entity.ToTable("Rumpfkrafttest");
            });

            // Entity mapping for Standweitsprung table
            modelBuilder.Entity<Standweitsprung>(entity =>
            {
                entity.ToTable("Standweitsprung");
            });

            // Entity mapping for Zwolfminutenlauf table
            modelBuilder.Entity<Zwolfminutenlauf>(entity =>
            {
                entity.ToTable("Zwolfminutenlauf");
            });
        }

    }
}
