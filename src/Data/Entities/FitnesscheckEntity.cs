namespace FitnessCheck.Data.Entities;

/// <summary>
/// Represents the entity class for storing data related to the Medizinballstoss.
/// </summary>
public class Medizinballstoss
{
    public int MedizinballstossID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch1 { get; set; }
    public float? Versuch2 { get; set; }
    public float? Versuch3 { get; set; }
    public float BesterVersuch { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}
/// <summary>
/// Represents the entity class for storing data related to the Einbeinstand.
/// </summary>
public class Einbeinstand
{
    public int EinbeinstandID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch1R { get; set; }
    public float? Versuch2R { get; set; }
    public float Versuch1L { get; set; }
    public float? Versuch2L { get; set; }
    public float BesterVersuchR { get; set; }
    public float BesterVersuchL { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}
/// <summary>
/// Represents the entity class for storing data related to the Pendellauf.
/// </summary>
public class Pendellauf
{
    public int PendellaufID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch1 { get; set; }
    public float? Versuch2 { get; set; }
    public float BesterVersuch { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}
/// <summary>
/// Represents the entity class for storing data related to the Rumpfkrafttest
/// </summary>
public class Rumpfkrafttest
{
    public int RumpfkrafttestID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}
/// <summary>
/// Represents the entity class for storing data related to the Standweitsprung.
/// </summary>
public class Standweitsprung
{
    public int StandweitsprungID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch1 { get; set; }
    public float Versuch2 { get; set; }
    public float Versuch3 { get; set; }
    public float BesterVersuch { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}
/// <summary>
/// Represents the entity class for storing data related to the Zwolfminutenlauf.
/// </summary>
public class Zwolfminutenlauf
{
    public int ZwolfminutenlaufID { get; set; }
    public string? SchuelerID { get; set; }
    public float Versuch { get; set; }
    public int Punkte { get; set; }
    public int AuthCode { get; set; }
}

