using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using FitnessCheck.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FitnessCheck.Data.Entities;

namespace FitnessCheck.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class FitnessController : Controller
{
    private readonly FitnessCheckDbContext _dbContext;

    public FitnessController(FitnessCheckDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    /// <summary>
    /// Endpoint for receiving Medizinballstoss results.
    /// </summary>
    /// <param name="data">The data containing the Medizinballstoss results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("medizinballstoss")]
    [Authorize]
    public async Task<IActionResult> MedizinballstossAsync([FromBody] Medizinballstoss data)
    {
        // Defining the values and points for Medizinballstoss
        var medizinballstoss = new List<(double Wert, int Punkte)>
        {
            (4.10, 1), (4.20, 2), (4.30, 3), (4.40, 4), (4.50, 5), (4.70, 6),
            (4.90, 7), (5.10, 8), (5.30, 9), (5.50, 10), (5.70, 11), (5.90, 12),
            (6.10, 13), (6.30, 14), (6.50, 15), (6.70, 16), (6.90, 17), (7.10, 18),
            (7.30, 19), (7.50, 20), (7.70, 21), (7.90, 22), (8.10, 23), (8.30, 24), (8.50, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Determining the best attempt
        float bestAttempt;
        if (data.Versuch2.HasValue && data.Versuch3.HasValue)
        {
            bestAttempt = new[] { data.Versuch1, data.Versuch2.Value, data.Versuch3.Value }.Max();
        }
        else if (data.Versuch2.HasValue)
        {
            bestAttempt = Math.Max(data.Versuch1, data.Versuch2.Value);
        }
        else if (data.Versuch3.HasValue)
        {
            bestAttempt = Math.Max(data.Versuch1, data.Versuch3.Value);
        }
        else
        {
            bestAttempt = data.Versuch1;
        }

        data.BesterVersuch = bestAttempt;

        // Find the highest Value that is less than or equal to bestAttempt
        var match = medizinballstoss.LastOrDefault(x => x.Wert <= bestAttempt + 0.001);
        int Punkte = match.Equals(default((double, int))) ? 0 : match.Punkte;

        data.Punkte = Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Medizinballstoss.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok($"Data saved successfully!");
    }

    /// <summary>
    /// Endpoint for receiving Einbeinstand results.
    /// </summary>
    /// <param name="data">The data containing the Einbeinstand results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("einbeinstand")]
    [Authorize]
    public async Task<IActionResult> EinbeinstandAsync([FromBody] Einbeinstand data)
    {
        // Defining the values and points for Einbeinstand
        var einbeinstand = new List<(int Wert, int Punkte)>
        {
            (11, 1), (14, 2), (17, 3), (20, 4), (23, 5),
            (26, 6), (29, 7), (31, 8), (33, 9), (35, 10),
            (37, 11), (39, 12), (41, 13), (43, 14), (45, 15),
            (47, 16), (49, 17), (51, 18), (54, 19), (58, 20),
            (64, 21), (71, 22), (79, 23), (88, 24), (100, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Determine the best attempt
        float bestAttemptR = data.Versuch2R.HasValue ? Math.Max(data.Versuch1R, data.Versuch2R.Value) : data.Versuch1R;
        data.BesterVersuchR = bestAttemptR;

        float bestAttemptL = data.Versuch2L.HasValue ? Math.Max(data.Versuch1L, data.Versuch2L.Value) : data.Versuch1L;
        data.BesterVersuchL = bestAttemptL;

        // Sum the best attempts for both left and right
        float totalBestAttempt = bestAttemptR + bestAttemptL;

        // Find the matching score for the total of the best right and left attempts
        var match = einbeinstand.LastOrDefault(x => x.Wert <= totalBestAttempt);
        data.Punkte = match.Equals(default((int, int))) ? 0 : match.Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Einbeinstand.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok("Data saved successfully!");
    }

    /// <summary>
    /// Endpoint for receiving Standweitsprung results.
    /// </summary>
    /// <param name="data">The data containing the Standweitsprung results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("standweitsprung")]
    [Authorize]
    public async Task<IActionResult> StandweitsprungAsync([FromBody] Standweitsprung data)
    {
        // Defining the values and points for Standweitsprung
        var standweitsprung = new List<(double Wert, int Punkte)>
        {
            (1.65, 1), (1.70, 2), (1.75, 3), (1.80, 4), (1.85, 5), (1.90, 6),
            (1.96, 7), (2.00, 8), (2.05, 9), (2.10, 10), (2.15, 11), (2.20, 12),
            (2.25, 13), (2.30, 14), (2.35, 15), (2.40, 16), (2.45, 17), (2.50, 18),
            (2.55, 19), (2.60, 20), (2.65, 21), (2.70, 22), (2.75, 23), (2.80, 24), (2.85, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Determine the best attempt
        float bestAttempt = new[] { data.Versuch1, data.Versuch2, data.Versuch3 }.Max();
        data.BesterVersuch = bestAttempt;

        // Find the matching score for the total of the best right and left attempts
        double epsilon = 0.001;
        var match = standweitsprung.LastOrDefault(x => x.Wert <= bestAttempt + epsilon);
        data.Punkte = match.Equals(default((double, int))) ? 0 : match.Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Standweitsprung.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok("Data saved successfully!");
    }

    /// <summary>
    /// Endpoint for receiving Rumpfkraft results.
    /// </summary>
    /// <param name="data">The data containing the Rumpfkraft results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("rumpfkrafttest")]
    [Authorize]
    public async Task<IActionResult> RumpfkrafttestAsync([FromBody] Rumpfkrafttest data)
    {
        // Defining the values and points for Rumpfkraft
        var rumpfkraft = new List<(double Wert, int Punkte)>
        {
            (0.05, 1), (0.10, 2), (0.15, 3), (0.20, 4), (0.25, 5),
            (0.30, 6), (0.40, 7), (0.50, 8), (1.00, 9),
            (1.10, 10), (1.20, 11), (1.30, 12), (1.40, 13), (1.50, 14),
            (2.00, 15), (2.10, 16), (2.25, 17), (2.40, 18), (2.55, 19),
            (3.10, 20), (3.30, 21), (3.50, 22), (4.10, 23), (4.30, 24), (4.50, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Find the matching score for the duration
        var match = rumpfkraft.LastOrDefault(x => x.Wert <= data.Versuch);
        data.Punkte = match.Equals(default((double, int))) ? 0 : match.Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Rumpfkrafttest.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok("Data saved successfully!");
    }

    /// <summary>
    /// Endpoint for receiving Zwolfminutenlauf results.
    /// </summary>
    /// <param name="data">The data containing the Zwolfminutenlauf results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("zwolfminutenlauf")]
    [Authorize]
    public async Task<IActionResult> ZwolfminutenlaufAsync([FromBody] Zwolfminutenlauf data)
    {
        // Defining the values and points for Zwolfminutenlauf
        var zwolfMinutenLauf = new List<(double Wert, int Punkte)>
        {
            (31, 1), (31.5, 2), (32, 3), (32.5, 4), (33, 5), (33.5, 6),
            (34.5, 7), (35, 8), (35.5, 9), (36, 10), (36.5, 11), (37.5, 12),
            (38, 13), (38.5, 14), (39, 15), (39.5, 16), (40.5, 17), (41, 18),
            (41.5, 19), (42, 20), (42.5, 21), (43.5, 22), (44, 23), (44.5, 24), (45, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Find the matching score for the achieved distance
        var match = zwolfMinutenLauf.LastOrDefault(x => x.Wert <= data.Versuch);
        data.Punkte = match.Equals(default((double, int))) ? 0 : match.Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Zwolfminutenlauf.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok("Data saved successfully!");
    }

    /// <summary>
    /// Endpoint for receiving Pendellauf results.
    /// </summary>
    /// <param name="data">The data containing the Pendellauf results.</param>
    /// <returns>An IActionResult indicating the status of the operation.</returns>
    [HttpPost("pendellauf")]
    [Authorize]
    public async Task<IActionResult> PendellaufAsync([FromBody] Pendellauf data)
    {
        // Defining the values and points for Pendellauf
        var pendellauf = new List<(double Wert, int Punkte)>
        {
            (12.26, 1), (12.12, 2), (11.98, 3), (11.84, 4), (11.70, 5),
            (11.56, 6), (11.42, 7), (11.28, 8), (11.14, 9), (11.00, 10),
            (10.86, 11), (10.72, 12), (10.58, 13), (10.44, 14), (10.30, 15),
            (10.16, 16), (10.02, 17), (9.88, 18), (9.74, 19), (9.60, 20),
            (9.46, 21), (9.32, 22), (9.18, 23), (9.04, 24), (8.90, 25)
        };

        // For this scenario, we use the email as en alternative to the SchuelerID
        var emailClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        if (string.IsNullOrEmpty(emailClaim))
        {
            return BadRequest("Email claim is missing from the JWT.");
        }

        // Determine the best attempt
        float bestAttempt = data.Versuch2.HasValue ? Math.Min(data.Versuch1, data.Versuch2.Value) : data.Versuch1;
        data.BesterVersuch = bestAttempt;

        // Calculate Points
        var match = pendellauf.OrderBy(x => x.Wert).FirstOrDefault(x => bestAttempt <= x.Wert);
        data.Punkte = match.Equals(default((double, int))) ? pendellauf.Max(x => x.Punkte) : match.Punkte;
        data.SchuelerID = emailClaim;

        // Add the data to the database context and save
        _dbContext.Pendellauf.Add(data);
        await _dbContext.SaveChangesAsync();

        return Ok("Data saved successfully!");
    }

}
