﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FitnessCheck.Migrations
{
    /// <inheritdoc />
    public partial class RenameDummyEntityFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Username",
                table: "DummyEntities",
                newName: "Email");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "DummyEntities",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "DummyEntities");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "DummyEntities",
                newName: "Username");
        }
    }
}
