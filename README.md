# Fitness Check

This service is a placeholder service as part of the GIBZ App backend. It's primary purpose is to serve as a starting point for students project works in module 300.

## Available features

The [src/](src/) folder contains a ASP.NET application based on .NET 8.0. The application is pretty basic and basically consists of just a single api controller named [DemoController](src/Controllers/v1/DemoController.cs). Within this controller you'll find an action which responds with HTTP status `200` to any authenticated request towards `/v1/demo`. The body of this response contains the unique identifier of the authenticated user.

The NuGet package `Microsoft.AspNetCore.Authentication.JwtBearer` is added to the project for enabling authentication using JWT (JSON web token). For a request to be authenticated, you must provide a JWT in the `Authorization` HTTP request header. In a productive environment, the user retrieves this JWT as part of the authentication. See section [How to authenticate (locally)](#how-to-authenticate-locally) below for further information regarding setting up the development environment.

## How to run the application (locally)

You might run this application within your favorite IDE (such as Visual Studio Code, Visual Studio, Rider, ...). Sooner or later you'll want to persist data in a designated database. For this purpose, the project contains a [Dockerfile](Dockerfile) (for building an image) along with a [compose.yaml](compose.yaml) file for running the application as part of a composition of containers - along with a database.

You may start the composition of two containers using this command on root level of this project:

```bash
# Start the container composition
docker compose up --build -d
```

While `docker compose up` along with the `-d` flag starts the composition, the `--build` flag enforces new images to be build before starting the containers. Omitting the `--build` flag would result in reusing pre-existing images - changes on your codebase would therefore _not_ be applied when restarting the container composition.

## How to authenticate (locally)

For local development purpose, you might use the `dotnet` cli tool `user-jwts` to generate long-living, local JWTs: `dotnet user-jwts create`. Upon executing this command, a signing key is generated and stored locally on your host machine. Since the key ist only available locally, validating signed JWT inside a container won't work. It's possible although to add a key for validating signed JWTs using the .NET configuration.

Specifically you may use the environment variables `Authentication__Schemes__Bearer__SigningKeys__0__Issuer` and `Authentication__Schemes__Bearer__SigningKeys__0__Value` which are already prepared in the [`compose.yaml`](compose.yaml) file.

```bash
# Read key from local file storage
dotnet user-jwts key
```

After the key is printed to the console, copy the key (without quotes) as value for the environment variable `Authentication__Schemes__Bearer__SigningKeys__0__Value` to the file `compose.yaml`.

## How to apply migrations

Since the application built in _Release_ mode and there are no dotnet tools (such as `dotnet ef`) available within the container, there's no possibility to apply migrations from within the container. Futhermore, the provided connection string (environment variable `ConnectionStrings__MariaDb`) uses the container name `fitness-check-db` as server name, it won't work from outside the docker composition, e.g. from your host machine.

The proposed solution to apply generated migrations to the database is by generating a SQL script which then might be executed within the database container. Stick to the following snippets which might be executed on the _Terminal_ in _Visual Studio Code_ starting on this projects root directory.

```bash
# Change to the src/ directory
cd src/

# Generate an idempotent migration and save the output to Migrations/migration.sql
dotnet ef migrations script --idempotent -o Migrations/migration.sql

# Copy the generated migration script into the container
docker cp Migrations/migration.sql fitness-check-db:/

# Execute the sql script in the container for database fitnesscheck
docker exec fitness-check-db /bin/sh -c 'mariadb -ugibzapp -ppassword fitnesscheck < /migration.sql'
```

The to final steps of the above script will only work if there's a container named `fitness-check-db` up and running.

All four steps together might be executed by running the script [`applyMigration.sh`](src/applyMigration.sh) on the terminal.
